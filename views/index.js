import React from "react";
import ReactDOM from "react-dom";

class WeatherWidget extends React.Component {

  constructor() {
    super();
    this.state = {
      weatherData: {
        cityName: '',
        temperature: 0,
        humidity: 0,
        windSpeed: 0,
        windDirection: '',
      },
      inputCityName: '',
      searchResults: []
    }

    this.handleCityChange = this.handleCityChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.getWeatherData = this.getWeatherData.bind(this);
  }

  handleCityChange(event) {
    this.setState({
      inputCityName: event.target.value
    })
  }

  handleSubmit(event) {
    event.preventDefault();
    const pageUrl = `?city=${this.state.inputCityName}`;
    window.history.pushState('', '', pageUrl);
    this.getWeatherData(this.state.inputCityName).then(weatherData => {
      this.setState(weatherData);
    });
  }

  getWeatherData(cityName) {
    return new Promise((resolve, reject) => {
      fetch(`/api?city=${cityName}`)
        .then(response => {
          return response.json();
        })
        .then(myJson => {
          resolve(myJson.weatherData);
        }).catch(e => {
          console.log(e);
          reject(e);
          // log frontend error
        });
    });
  }

  componentDidMount() {
    const urlParams = new URLSearchParams(window.location.search);
    let firstLocation;

    if (urlParams.has('city')) {
      firstLocation = urlParams.get('city')
    } else {
      firstLocation = this.props.defaultLocation;
    }

    this.getWeatherData(firstLocation).then(weatherData => {
      this.setState(weatherData);
    });
  }

  render() {
    let { cityName, temperature, humidity, windSpeed, windDirection } = this.state;

    return <div>
      <div className="panel-heading">Weather in <b>{cityName}</b></div>
      <ul className="list-group">
        <li className="list-group-item">Temperature: <b>{temperature}°C</b></li>
        <li className="list-group-item">Humidity: <b>{humidity}</b></li>
        <li className="list-group-item">Wind: <b>{windSpeed} m/s {windDirection}</b></li>
        <li className="list-group-item">
          <form className="form-inline" method="GET">
            <div className="form-group">
              <input
                type="text"
                className="form-control"
                id="city"
                placeholder="City"
                onChange={this.handleCityChange}
              />
            </div>
            <button type="submit" className="btn btn-default" onClick={this.handleSubmit}>Search</button>
          </form>
        </li>
      </ul>
    </div>

  }
}

const mountNode = document.getElementById("weather");

ReactDOM.render(<WeatherWidget defaultLocation="Copenhagen" />, mountNode);
