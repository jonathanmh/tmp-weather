const express = require('express');
const app = express();
const r2 = require('r2')

const WEATHER_API_KEY = '166d00e26d3ff2c6149e89feccc5c59a';
const COUNTRY_CODE = 'dk'; // ISO 316

/**
 * 
 * @param {string} cityName 
 * @returns {Promise<Object>} weatherData
 */
function getCityWeather(cityName) {
  return new Promise(async (resolve, reject) => {

    const weatherData = {
      cityName: undefined,
      humidity: undefined,
      windSpeed: undefined,
      windDirection: undefined,
      temperature: undefined
    }

    try {
      let weatherResponse = await r2.get(`https://api.openweathermap.org/data/2.5/weather?q=${encodeURIComponent(cityName)},${COUNTRY_CODE}&units=metric&appid=${WEATHER_API_KEY}`).json
      weatherData.humidity = weatherResponse.main.humidity;
      weatherData.temperature = weatherResponse.main.temp;
      weatherData.windSpeed = weatherResponse.wind.speed;
      weatherData.windDirection = getWindDirection(weatherResponse.wind.deg);
      weatherData.cityName = cityName;
      resolve(weatherData);
    } catch (openWeatherApiError) {
      console.log(openWeatherApiError);
      reject(openWeatherApiError)
    }
  });
}

/**
 * 
 * @param {number} degree 
 * @returns {string} localised wind direction
 * more info: http://snowfence.umn.edu/Components/winddirectionanddegreeswithouttable3.htm
 */
function getWindDirection(degree) {
  let direction = '';
  switch (true) {
    case (degree > 326.25 || degree <= 33.75):
      direction = 'Nord';
      break;
    case (degree > 33.75 || degree <= 123.75):
      direction = 'Øst';
      break;
    case (degree > 123.75 || degree <= 236.25):
      direction = 'Syd';
      break;
    default:
      direction = 'Vest';
      break;
  }
  return direction;
}

app.set('view engine', 'ejs');
app.use(express.static('views/dist'))

app.get('/', function (req, res) {
  const cityQueryVar = req.query.city;
  let cityName = '';
  if (typeof cityQueryVar === 'undefined' || cityQueryVar.length === 0) {
    cityName = 'Copenhagen'
  } else {
    cityName = cityQueryVar;
  }

  getCityWeather(cityName).then(weatherData => {
    res.render('index', { weatherData });
  }).catch(e => {
    getCityWeather('Copenhagen').then(defaultData => {
      res.render('index', { weatherData: defaultData });
    })
  })
});

app.get('/api', function (req, res) {
  const cityQueryVar = req.query.city;
  let cityName = '';
  if (typeof cityQueryVar === 'undefined' || cityQueryVar.length === 0) {
    cityName = 'Copenhagen'
  } else {
    cityName = cityQueryVar;
  }

  getCityWeather(cityName).then(weatherData => {
    res.json({ status: 'ok', weatherData });
  }).catch(e => {
    res.status(500);
    res.json({ status: 'error' });
  })
});


module.exports = app;
