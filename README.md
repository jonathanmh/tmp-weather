# Weather Widget

## Currently Implemented

- functionality both as HTML form and as React component
- minimal integration test for backend `app.js` -> `app.test.js`

## ToDo:

- Autocomplete search in react
- unit test where possible in backend
- tests for frontend component
- write Dockerfile that installs dependencies and builds frontend
- move server `PORT` and openweather `API_KEY` to environment variables
- (Optimisation: re-hydrate react component with SSR data)

## Usage

- Development: `npm run dev`
- Test: `npm run test`
