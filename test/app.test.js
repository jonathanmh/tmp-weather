const request = require('supertest');
var expect = require('chai').expect;
const app = require('../app');

describe('GET /', () => {
  it('responds with html', done => {
    request(app)
      .get('/')
      .expect('Content-Type', /html/)
      .expect(200)
      .end(done)
  });
});

describe('GET /api', () => {
  it('responds with json', done => {
    request(app)
      .get('/api')
      .expect('Content-Type', /json/)
      .expect(200)
      .expect(function (res) {
        // test shape of json response
        expect(Object.keys(res.body.weatherData)).to.deep.equal(['cityName', 'humidity', 'windSpeed', 'windDirection', 'temperature']);
      })
      .end(done)
  });
});